FROM freqtradeorg/freqtrade:stable
LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"

USER ftuser
RUN pip install --user py3cw
RUN curl https://raw.githubusercontent.com/AlexBabescu/freqtrade_3commas/master/user_data/freqtrade3cw.py -o /freqtrade/user_data/freqtrade3cw.py

ENTRYPOINT ["freqtrade"]
CMD ["trade"]
